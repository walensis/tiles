Algorithmique Avancée : Devoir 3
================================

Le problème du chargement de palettes
--------------------------------------------------------
**Dépôt Bitbucket :** [Lien](https://bitbucket.org/walensis/tiles.git)

## Team
Soufiane BENNANI, Hamza EL MEKNASSI, Majda BOUGHANMI.

## Synopsis
Nous disposons de palettes de cartons de savons de Marseille contrefaits que nous souhaitons expédier
dans  des  conteneurs  depuis  un  port  de  Chine  vers  le  grand  port  maritime  de  Marseille.  Le  coût  du
transport  dépend  uniquement  du  nombre  de  conteneurs  utilisés,  donc  l’appât  du  gain  nous  incite  à
mettre le plus grand nombre possible de palettes dans chaque conteneur, pour minimiser le nombre de
conteneurs  utilisés.  Tous  les  conteneurs  sont  de  forme  rectangulaire  et  de  dimensions  identiques.  Les
palettes ont aussi une base rectangulaire et ne peuvent pas être superposées. Nous allons donner une
heuristique basée sur la programmation dynamique pour résoudre ce problème. On appelle heuristique
un algorithme pour un problème d’optimisation qui calcule une solution sans garantir qu’elle est optimale.

## Usage
...

## Exécution
...