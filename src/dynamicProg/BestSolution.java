package dynamicProg;

import java.util.HashMap;
import java.util.List;

import geometry.EllShape;
import geometry.Packing;
import geometry.Subdivision;
import geometry.Tile;

/* The class BestSolution encodes the best solution found so far
   on a specific L-shape, for a given tile. It has three methods:

   - isOptimal checks whether the current known solution is sure to be optimal,

   - accept takes a subdivision of the L-shape and try to replace the current
     solution by a solution based on the two parts of that subdivision,

   - best returns the current best known solution

  It is mostly made to implement the computation of the maximum
  in the recursive formula for the maximum packing. 

  DONE: add fields and constructors.
  DONE: use a greedy packing to initialize the solution.
  TODO: implement the three methods.
 */


class BestSolution {
	private Packing bestSoFar;
	private Subdivision bestSubdivision;
	private EllShape ground;
	private Tile tile;
	//Should it be in the DynamicProgrammingSolver?
	public HashMap<Integer, Packing> memo;
	
	public BestSolution(EllShape L, Tile basicTile) {
		this.bestSoFar = L.packGreedily(basicTile);
		this.bestSubdivision = null;
		this.tile = basicTile;
		this.ground = L;
		this.memo = new HashMap<Integer, Packing>();
	}

	public boolean isOptimal() { 
		/*Considering an optimal solution does not leave enough
		room to fit another tile */
		int roomLeft = ground.getArea() - (tile.getArea()*bestSoFar.size());
		//DEBUG
		System.out.println("Room left :"+roomLeft+" -- 1 tile area :"+tile.getArea());
		return (roomLeft > tile.getArea()+1)? false:true;
	}
	
	public void accept(Subdivision subdivision) {
		// Part 1
		//Check if we already computed to optimal packing for the shape
		Packing part1 = memo.get(subdivision.part1.shape.hashCode());
		
		if (part1 == null) { //if not
			//Generate a packing at (0,0)
			part1 = subdivision.part1.shape.packGreedily(tile);
			//Store it in the hash map so we don't have to regenerate it again
			memo.put(subdivision.part1.shape.hashCode(), part1.clone());
		}
		//Move all the tiles of the newly generated packing relatively to the part position and rotation
		part1.selfTransform(subdivision.part1);
		
		// Part 2
		//Same
		Packing part2 = memo.get(subdivision.part2.shape.hashCode());
		
		if (part2 == null) {
			part2 = subdivision.part2.shape.packGreedily(tile);
			memo.put(subdivision.part2.shape.hashCode(), part2.clone());
		}
		part2.selfTransform(subdivision.part2);
		
		Packing whole = Packing.concat(part1, part2);
		
		if (bestSoFar.size() < whole.size()) {
			bestSoFar = whole.clone();
			bestSubdivision = new Subdivision(subdivision.part1, subdivision.part2);
		}
			
	}

	public Packing best() { return bestSoFar; }
	
	//Return the best subdivision corresponding to the best solution so far
	public Subdivision bestSub() { return bestSubdivision; } 

}
