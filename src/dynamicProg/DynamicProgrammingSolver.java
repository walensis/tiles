package dynamicProg;

/* DynamicProgrammingSolver is the class implementing the dynamic program.

  TODO: add structure to memoise computed values.
  TODO: complete class BestSolution.
  TODO: encode recursion using stream of subdivisions (computeOptimalSolution).
  TODO: retrieveOptimalSolution (access memoised value or compute solution)
  TODO: solve
 */

import geometry.EllShape;
import geometry.Packing;
import geometry.Subdivision;
import geometry.Tile;

import java.util.Iterator;
import java.util.stream.Stream;


public class DynamicProgrammingSolver {
	public final EllShape ground;
	public final Tile basicTile;
	private BestSolution solution;

	public DynamicProgrammingSolver(EllShape ground, Tile basicTile) {
		this.ground = ground;
		this.basicTile = basicTile;
		this.solution = new BestSolution(ground, basicTile);
	}

	private Packing computeOptimalSolution(EllShape shape) {
		Stream<Subdivision> subStream = shape.subdivisions();
		/* DEBUG */
		int tested = 0;
		int badSubs = 0;
		/* FIN DEBUG */
		Iterator<Subdivision> i = subStream.iterator();
		while (i.hasNext() && !solution.isOptimal()) {
			Subdivision sub = i.next();
			if (sub.part1.shape.height+sub.part2.shape.height == shape.height ||
					sub.part1.shape.width+sub.part2.shape.width == shape.width) {
				solution.accept(sub);
				tested++;
			} else {
				/* DEBUG */
				badSubs++;
				System.out.println("--!-- Caught bad subdivision --"+badSubs+"--");
				/* FIN DEBUG */
			}
			
		}
		/* DEBUG */
		System.out.println("Total subdivisions tested "+tested);
		/* FIN DEBUG */
		if (!solution.isOptimal()) { // If after trying all subs it's still not optimal
			Packing pack1 = computeOptimalSolution(solution.bestSub().part1.shape);
			Packing pack2 = computeOptimalSolution(solution.bestSub().part2.shape);
			return Packing.concat(pack1, pack2);
		}
		
		return solution.best();
	}
	
	//Retrieve optimal solution either from memo or compute it?
	public Packing retrieveOptimalSolution(EllShape shape) {
		/*Packing p = solution.memo.get(shape.hashCode());
		return (p == null)? computeOptimalSolution(shape):p;*/
		return computeOptimalSolution(shape);
	}

	public Packing solve() {
		return retrieveOptimalSolution(ground);
	}

}
